import java.util.Scanner;

public class MoveIt {
	/*
	 * by :- Killerbot225
	 */

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String command = "";
		Thread moverThread = null;
		Thread hitterThread = null;
		Mover mover = null;
		Hitter hitter = null;

		while (!command.equalsIgnoreCase("quit")) {
			System.out.print("Choose action (Start/Stop/quit) : ");
			command = in.nextLine();

			if (command.equalsIgnoreCase("start")) {
				if (moverThread == null || !moverThread.isAlive()) {
					mover = new Mover();
					hitter = new Hitter();
					// moverThread = new Thread(mover);
					hitterThread = new Thread(hitter);
					// moverThread.start();
					hitterThread.start();
					System.out.println("Process starting");
				}else {
					System.out.println("Process already running");
				}
			} else if (command.equalsIgnoreCase("stop")) {
				if (moverThread != null && moverThread.isAlive()) {
					mover.stop.set(true);
					System.out.println("Process stopping");
				}
				
				if (hitterThread != null && hitterThread.isAlive()) {
					hitter.stop.set(true);
					System.out.println("Process stopping");
				}
			} else if (command.equalsIgnoreCase("quit")) {
				System.exit(0);
			} else {
				System.out.println("Invalid command");
			}

		}
		
		in.close();
	}

}
