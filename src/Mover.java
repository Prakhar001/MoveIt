import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class Mover implements Runnable {
	/*
	 * by :- Killerbot225
	 */

	public AtomicBoolean stop = new AtomicBoolean(false);

	@Override
	public void run() {
		while (!stop.get()) {
			try {
				int x, y;
				Robot robot = new Robot();
				Random r = new Random();
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				Point point[] = new Point[20];
				Point current = new Point(0, 0);
				Point next = new Point(0, 0);

				x = (int) screenSize.getWidth();
				y = (int) screenSize.getHeight();

				for (int i = 0; i < point.length; i++) {
					point[i] = new Point(r.nextInt(x), r.nextInt(y));
				}

				for (int i = 0; i < point.length && !stop.get(); i++) {
					Point target = point[i];
					current = MouseInfo.getPointerInfo().getLocation();
					next = nextPos(current, target);

					while (!current.equals(next) && !stop.get()) {
						current = MouseInfo.getPointerInfo().getLocation();
						next = nextPos(current, target);
						robot.mouseMove(next.x, next.y);
						robot.delay(1);
					}
				}

				Thread.sleep(r.nextInt(15000));

			} catch (AWTException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
				stop.set(true);
			}

		}
	}

	private Point nextPos(Point current, Point target) {
		Point pos = new Point(current);
		if (current.x < target.x) {
			pos.x++;
		}
		if (current.y < target.y) {
			pos.y++;
		}
		if (current.x > target.x) {
			pos.x--;
		}
		if (current.y > target.y) {
			pos.y--;
		}
		return pos;
	}

}
