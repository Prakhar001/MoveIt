import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class Hitter implements Runnable {
	/*
	 * by :- Killerbot225
	 */
	public AtomicBoolean stop = new AtomicBoolean(false);

	@Override
	public void run() {
		try {
			/*Update this list*/
			String phrases[] = { "for(i=0;i<arr.length;i++) \n{\n\n}", "type it type it type it",
					"Once upon a time there was an ugly barnacle" };
			
			
			HashMap<Character, Integer> keys = new HashMap<>();
			HashMap<Character, Integer> shiftKeys = new HashMap<>();

			keys.put('a', KeyEvent.VK_A);
			keys.put('b', KeyEvent.VK_B);
			keys.put('c', KeyEvent.VK_C);
			keys.put('d', KeyEvent.VK_D);
			keys.put('e', KeyEvent.VK_E);
			keys.put('f', KeyEvent.VK_F);
			keys.put('g', KeyEvent.VK_G);
			keys.put('h', KeyEvent.VK_H);
			keys.put('i', KeyEvent.VK_I);
			keys.put('j', KeyEvent.VK_J);
			keys.put('k', KeyEvent.VK_K);
			keys.put('l', KeyEvent.VK_L);
			keys.put('m', KeyEvent.VK_M);
			keys.put('n', KeyEvent.VK_N);
			keys.put('o', KeyEvent.VK_O);
			keys.put('p', KeyEvent.VK_P);
			keys.put('q', KeyEvent.VK_Q);
			keys.put('r', KeyEvent.VK_R);
			keys.put('s', KeyEvent.VK_S);
			keys.put('t', KeyEvent.VK_T);
			keys.put('u', KeyEvent.VK_U);
			keys.put('v', KeyEvent.VK_V);
			keys.put('w', KeyEvent.VK_W);
			keys.put('x', KeyEvent.VK_X);
			keys.put('y', KeyEvent.VK_Y);
			keys.put('z', KeyEvent.VK_Z);
			keys.put(' ', KeyEvent.VK_SPACE);
			keys.put('\'', KeyEvent.VK_SLASH);
			keys.put(';', KeyEvent.VK_SEMICOLON);
			keys.put('"', KeyEvent.VK_QUOTEDBL);
			keys.put('=', KeyEvent.VK_EQUALS);
			keys.put('+', KeyEvent.VK_PLUS);
			keys.put('-', KeyEvent.VK_MINUS);
			keys.put('0', KeyEvent.VK_0);
			keys.put('1', KeyEvent.VK_1);
			keys.put('2', KeyEvent.VK_2);
			keys.put('3', KeyEvent.VK_3);
			keys.put('4', KeyEvent.VK_4);
			keys.put('5', KeyEvent.VK_5);
			keys.put('6', KeyEvent.VK_6);
			keys.put('7', KeyEvent.VK_7);
			keys.put('8', KeyEvent.VK_8);
			keys.put('9', KeyEvent.VK_9);
			keys.put('[', KeyEvent.VK_OPEN_BRACKET);
			keys.put(']', KeyEvent.VK_CLOSE_BRACKET);
			keys.put('.', KeyEvent.VK_PERIOD);
			keys.put('\n', KeyEvent.VK_ENTER);

			shiftKeys.put('A', KeyEvent.VK_A);
			shiftKeys.put('B', KeyEvent.VK_B);
			shiftKeys.put('C', KeyEvent.VK_C);
			shiftKeys.put('D', KeyEvent.VK_D);
			shiftKeys.put('E', KeyEvent.VK_E);
			shiftKeys.put('F', KeyEvent.VK_F);
			shiftKeys.put('G', KeyEvent.VK_G);
			shiftKeys.put('H', KeyEvent.VK_H);
			shiftKeys.put('I', KeyEvent.VK_I);
			shiftKeys.put('J', KeyEvent.VK_J);
			shiftKeys.put('K', KeyEvent.VK_K);
			shiftKeys.put('L', KeyEvent.VK_L);
			shiftKeys.put('M', KeyEvent.VK_M);
			shiftKeys.put('N', KeyEvent.VK_N);
			shiftKeys.put('O', KeyEvent.VK_O);
			shiftKeys.put('P', KeyEvent.VK_P);
			shiftKeys.put('Q', KeyEvent.VK_Q);
			shiftKeys.put('R', KeyEvent.VK_R);
			shiftKeys.put('S', KeyEvent.VK_S);
			shiftKeys.put('T', KeyEvent.VK_T);
			shiftKeys.put('U', KeyEvent.VK_U);
			shiftKeys.put('V', KeyEvent.VK_V);
			shiftKeys.put('W', KeyEvent.VK_W);
			shiftKeys.put('X', KeyEvent.VK_X);
			shiftKeys.put('Y', KeyEvent.VK_Y);
			shiftKeys.put('Z', KeyEvent.VK_Z);
			
			shiftKeys.put('{', KeyEvent.VK_BRACELEFT);
			shiftKeys.put('}', KeyEvent.VK_BRACERIGHT);
			shiftKeys.put('(', KeyEvent.VK_LEFT_PARENTHESIS);
			shiftKeys.put(')', KeyEvent.VK_RIGHT_PARENTHESIS);
			shiftKeys.put('<', KeyEvent.VK_LESS);
			shiftKeys.put('>', KeyEvent.VK_GREATER);

			Robot robot = new Robot();

			while (!stop.get()) {
				robot.delay(5000);
				Random r = new Random();

				String text = phrases[r.nextInt(phrases.length)];

				for (char c : text.toCharArray()) {
					if (keys.containsKey(c)) {
						robot.keyPress(keys.get(c));
						robot.keyRelease(keys.get(c));
						robot.delay(150);
						// System.out.println(c);
					} else if (shiftKeys.containsKey(c)) {
						robot.keyPress(KeyEvent.VK_SHIFT);
						robot.keyPress(shiftKeys.get(c));
						robot.keyRelease(shiftKeys.get(c));
						robot.keyRelease(KeyEvent.VK_SHIFT);
						robot.delay(250);
					}
				}
				robot.delay(2000);
				for (int i = 0; i < text.length(); i++) {
					robot.keyPress(KeyEvent.VK_BACK_SPACE);
					robot.keyRelease(KeyEvent.VK_BACK_SPACE);
					robot.delay(3);
				}
			}
		} catch (AWTException e) {
			e.printStackTrace();
		}

	}

}
